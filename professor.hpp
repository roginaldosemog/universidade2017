#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP

#include "pessoa.hpp"
#include <string>

class Professor : public Pessoa {

//Atributos
private:
	string formacao;
	float salario;
	string sala;

//Métodos
public:
	Professor();
	~Professor();
	void setFormacao(string formacao);
	string getFormacao();
	void setSalario(float salario);
	float getSalario();
	void setSala(string sala);
	string getSala();

	void imprimeDadosProfessor();
	
};


#endif