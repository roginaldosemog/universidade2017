#include <iostream>
#include "pessoa.hpp"
#include "aluno.hpp"
#include "professor.hpp"

using namespace std;

int main(int argc, char ** argv) {

/*
   // Criação de objetos com alocação estática
   Pessoa pessoa_1;
   Pessoa pessoa_2("Maria", "555-1111", 21);

   pessoa_1.setNome("Joao");
   pessoa_1.setMatricula("14/0078070");
   pessoa_1.setTelefone("555-5555");
   pessoa_1.setSexo("M");
   pessoa_1.setIdade(20);

   // Criação de objetos com alocação dinâmica
   Pessoa * pessoa_3;
   pessoa_3 = new Pessoa();
   Pessoa * pessoa_4;
   pessoa_4 = new Pessoa("Marcelo", "666-7777", 25);   
  
   pessoa_3->setNome("Pateta");
   pessoa_3->setMatricula("10/12312313");
   pessoa_3->setTelefone("4444-1111");
   pessoa_3->setSexo("M");
   pessoa_3->setIdade(12);
   
   // Impressão dos atributos dos objetos utilizando método da própria classe
   pessoa_1.imprimeDados();
   pessoa_2.imprimeDados();
   pessoa_3->imprimeDados();
   pessoa_4->imprimeDados();

   // Criação de objeto da classe filha
   Aluno aluno_1;
   aluno_1.setNome("João");
   cout << "Curso do aluno " << aluno_1.getNome() << ": " << aluno_1.getCurso() << endl;

   // Exemplo de cadastro de uma lista de Alunos
   Aluno *lista_de_alunos[20]; // Criação da lista de tamanho fixo de ponteiros do objeto Aluno 

   string nome;
   string matricula;
   int idade;
   string sexo;
   string telefone;
   float ira;
   int semestre;
   string curso;

   // Entrada de dados do terminal (stdin)
   cout << "Nome: ";
   cin >> nome;
   cout << "Matricula: ";
   cin >> matricula;
   cout << "Idade: ";
   cin >> idade;

   // Criação do primeiro objeto da lista
   lista_de_alunos[0] = new Aluno();

   // Definição dos atributos do objeto
   lista_de_alunos[0]->setNome(nome);
   lista_de_alunos[0]->setMatricula(matricula);
   lista_de_alunos[0]->setIdade(idade);

   // Impressão dos atributos do objeto
   lista_de_alunos[0]->imprimeDadosAluno();


   // Liberação de memória dos objetos criados dinamicamente
   delete(pessoa_3);
   delete(pessoa_4);
*/

   Aluno *lista_de_alunos[21];
   Professor *lista_de_professores[21];

   string nome;
   string matricula;
   int idade;
   string sexo;
   string telefone;
   float ira;
   int semestre;
   string curso;
   string formacao;
   float salario;
   string sala;

   int i, qtdCadastros, alunosCadastrados, professoresCadastros = 0;

   //Cadastro UnB
   cout << "Cadastro UnB - Alunos e Professores" << endl;
   cout << endl << "Deseja cadastrar quantos alunos? (Valor entre 0 e 20):  ";
   cin >> alunosCadastrados;
   while(alunosCadastrados<0 || alunosCadastrados>20){
      cout << "Valor invalido!";
      cout << "Deseja cadastrar quantos alunos? (Valor entre 0 e 20):  ";
      cin >> alunosCadastrados;
   }
   for(i=1; i<=alunosCadastrados; i++){
      cout << endl << "Cadastro Aluno " << i << " : " << endl;
      cout << "Nome: ";
      cin >> nome;
      cout << "Matricula: ";
      cin >>  matricula;
      cout << "Idade: ";
      cin >> idade;
      cout << "Sexo: ";
      cin >> sexo;
      cout << "Telefone: ";
      cin >> telefone;
      cout << "IRA: ";
      cin >> ira;
      cout << "Semestre: ";
      cin >> semestre;
      cout << "Curso: ";
      cin >> curso;
      lista_de_alunos[i] = new Aluno();
      lista_de_alunos[i]->setNome(nome);
      lista_de_alunos[i]->setMatricula(matricula);
      lista_de_alunos[i]->setIdade(idade);
      lista_de_alunos[i]->setSexo(sexo);
      lista_de_alunos[i]->setTelefone(telefone);
      lista_de_alunos[i]->setIra(ira);
      lista_de_alunos[i]->setSemestre(semestre);
      lista_de_alunos[i]->setCurso(curso);
   }

   cout << endl << "Deseja cadastrar quantos professores? (Valor entre 0 e 20):  ";
   cin >> professoresCadastros;
   while(professoresCadastros<0 || professoresCadastros>20){
      cout << "Valor invalido!";
      cout << "Deseja cadastrar quantos alunos? (Valor entre 0 e 20):  ";
      cin >> professoresCadastros;
   }
   for(i=1; i<=professoresCadastros; i++){
      cout << endl << "Cadastro Professor " << i << " : " << endl;
      cout << "Nome: ";
      cin >> nome;
      cout << "Matricula: ";
      cin >>  matricula;
      cout << "Idade: ";
      cin >> idade;
      cout << "Sexo: ";
      cin >> sexo;
      cout << "Telefone: ";
      cin >> telefone;
      cout << "Formacao: ";
      cin >> formacao;
      cout << "Salario: ";
      cin >> salario;
      cout << "Sala: ";
      cin >> sala;
      lista_de_professores[i] = new Professor();
      lista_de_professores[i]->setNome(nome);
      lista_de_professores[i]->setMatricula(matricula);
      lista_de_professores[i]->setIdade(idade);
      lista_de_professores[i]->setSexo(sexo);
      lista_de_professores[i]->setTelefone(telefone);
      lista_de_professores[i]->setFormacao(formacao);
      lista_de_professores[i]->setSalario(salario);
      lista_de_professores[i]->setSala(sala);
   }

   //Imprimir cadastros
   cout << endl << "## ALUNOS CADASTRADOS ##" << endl;
   for(i=1; i<=alunosCadastrados; i++){
      cout << endl << "ALUNO " << i << endl;
      cout << "Nome: " << nome << endl;
      cout << "Matricula: " << matricula << endl;
      cout << "Idade: " << idade << endl;
      cout << "Sexo: " << sexo << endl;
      cout << "Telefone: " << telefone << endl;
      cout << "IRA: " << ira  << endl;
      cout << "Semestre: " << semestre << endl;
      cout << "Curso: " << curso << endl;
   }

   cout << endl << "## PROFESSORES CADASTRADOS ##" << endl;
   for(i=1; i<=professoresCadastros; i++){
      cout << endl << "PROFESSOR " << i << endl;
      cout << "Nome: " << nome << endl;
      cout << "Matricula: " << matricula << endl;
      cout << "Idade: " << idade << endl;
      cout << "Sexo: " << sexo << endl;
      cout << "Telefone: " << telefone << endl;
      cout << "Formacao: " << formacao  << endl;
      cout << "Salario: " << salario << endl;
      cout << "Sala: " << sala << endl;
   }

   cout << endl;
   //delete(lista_de_alunos[21]);
   //delete(lista_de_professores[21]);

   return 0;
}
