#include "professor.hpp"
#include <iostream>

using namespace std;

Professor::Professor(){
	setNome("");
	setMatricula("");
  	setIdade(0);
  	setSexo("");
  	setTelefone("");
  	setFormacao("");
  	setSalario(0.0);
  	setSala("");
}

Professor::~Professor(){}

void Professor::setFormacao(string formacao){
	this->formacao = formacao;
}
string Professor::getFormacao(){
	return formacao;
}
void Professor::setSalario(float salario){
	this->salario = salario;
}
float Professor::getSalario(){
	return salario;
}
void Professor::setSala(string sala){
	this->sala = sala;
}
string Professor::getSala(){
	return sala;
}

void Professor::imprimeDadosProfessor(){
	cout << "Dados do Professor" << endl;
	imprimeDados();
	cout << "Formacao: " << getFormacao() << endl;
	cout << "Salario: " << getSalario() << endl;
	cout << "Sala: " << getSala() << endl;
}